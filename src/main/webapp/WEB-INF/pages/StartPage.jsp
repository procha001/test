<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         isELIgnored="false"
%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>StartPage</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <br>
    <h2>Select method and input parameter for search rooms.</h2>
    </br>
    <br>
    <form action="/startpage" method="get">
        <div class="form-group">
            <label for="guestName"></label>
            <input type="text" class="form-control" id="guestName" placeholder="Enter Guest name"
                   name="guestName">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <form action="/startpage" method="get">
        <div class="form-group">
            <label for="coastMin"></label>
            <input type="number" class="form-control" id="coastMin" placeholder="Enter min coast"
                   name="coastMin">
        </div>
        <div class="form-group">
            <label for="coastMax"></label>
            <input type="number" class="form-control" id="coastMax" placeholder="Enter max coast"
                   name="coastMax">
        </div>
        <div class="form-group">
            <label for="quantityMan"></label>
            <input type="number" class="form-control" id="quantityMan" placeholder="Enter quantity man"
                   name="quantityMan">
        </div>
        <div class="form-group">
            <label for="rating"></label>
            <input type="number" class="form-control" id="rating" placeholder="Enter min hotel's rating"
                   name="rating">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    </br>
    <br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th><h4>Number</h4></th>
            <th><h4>Description</h4></th>
            <th><h4>Coast</h4></th>
            <th><h4>Shower</h4></th>
            <th><h4>Tv</h4></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="room" items="${rooms}">
            <tr>
                <td><h4>${room.number}</h4></td>
                <td><h4>${room.description}</h4></td>
                <td><h4>${room.coast}</h4></td>
                <td><h4>${room.shower}</h4></td>
                <td><h4>${room.tv}</h4></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </br>
</div>
</body>
</html>
