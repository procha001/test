package com.gmail.procha001.repository;

import com.gmail.procha001.repository.model.Guest;

public interface GuestDao extends GenericDao<Guest, Long> {
}
