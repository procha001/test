package com.gmail.procha001.repository;

import java.io.Serializable;

public interface GenericDao<T extends Serializable, ID extends Serializable> {
    ID save(T entity);

    void saveOrUpdate(T entity);

    void delete(T entity);

    T find(ID id);
}
