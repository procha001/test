package com.gmail.procha001.repository;

import com.gmail.procha001.repository.model.Hotel;

public interface HotelDao extends GenericDao<Hotel, Long> {
}
