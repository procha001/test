package com.gmail.procha001.repository;

import com.gmail.procha001.repository.model.Room;

import java.util.List;


public interface RoomDao extends GenericDao<Room, Long> {
    void save(List<Room> rooms);

    List<Room> find(Long coastMin, Long coastMax, Integer quantityMan, Integer rating);

    List<Room> findByGuest(String name);
}
