package com.gmail.procha001.repository.impl;

import com.gmail.procha001.controller.StartServlet;
import com.gmail.procha001.repository.RoomDao;
import com.gmail.procha001.repository.model.Room;
import com.gmail.procha001.repository.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class RoomDaoImpl extends GenericDaoImpl<Room, Long> implements RoomDao {
    private static final Logger logger = Logger.getLogger(RoomDaoImpl.class);
    private static RoomDaoImpl instance;

    public static RoomDaoImpl getInstance() {
        if (instance == null) {
            instance = new RoomDaoImpl();
        }
        return instance;
    }

    private RoomDaoImpl() {
    }

    public void save(List<Room> rooms) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            for (Room room : rooms) {
                session.save(room);
            }
            tx.commit();
            logger.info("End transaction save <Room>");
        } catch (Exception e) {
            logger.error("Error: " + e.toString());
            if (tx != null) {
                tx.rollback();
            }
        }
    }

    public List<Room> find(Long coastMin, Long coastMax, Integer quantityMan, Integer rating) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Room> rooms = null;
        if (coastMin != null && coastMax != null && quantityMan != null && rating != null) {
            try {
                tx = session.beginTransaction();
                Query query = session.createQuery("from Room r where r.coast>=:coastMin and r.coast<=:coastMax and r.roomInfo.quantityMan=:quantityMan and hotel.rating>=:rating");
                query.setParameter("coastMin", coastMin);
                query.setParameter("coastMax", coastMax);
                query.setParameter("quantityMan", quantityMan);
                query.setParameter("rating", rating);
                rooms = query.list();
                tx.commit();
                logger.info("End transaction find <Room>");
            } catch (Exception e) {
                logger.error("Error: " + e.toString());
                if (tx != null) {
                    tx.rollback();
                }
            }
        }
        return rooms;
    }

    public List<Room> findByGuest(String name) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = null;
        List<Room> rooms = null;
        if (name != null) {
            try {
                tx = session.beginTransaction();
                Query query = session.createQuery("from Room r join fetch r.guests g where g.name=:name and r.hotel.rating>=5 and r.hotel.rating<=8");
                query.setParameter("name", name);
                rooms = query.list();
                tx.commit();
            } catch (Exception e) {
                logger.error("Error: " + e.toString());
                if (tx != null) {
                    tx.rollback();
                }
            }
        }
        logger.info("End transaction findByGuest List<Room>");
        return rooms;
    }
}