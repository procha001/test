package com.gmail.procha001.repository.impl;

import com.gmail.procha001.repository.RoomInfoDao;
import com.gmail.procha001.repository.model.RoomInfo;

public class RoomInfoDaoImpl extends GenericDaoImpl<RoomInfo, Long> implements RoomInfoDao {
    private static RoomInfoDaoImpl instance;

    public static RoomInfoDaoImpl getInstance() {
        if (instance == null) {
            instance = new RoomInfoDaoImpl();
        }
        return instance;
    }

    private RoomInfoDaoImpl() {
    }
}
