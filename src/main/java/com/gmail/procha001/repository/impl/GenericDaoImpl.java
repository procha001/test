package com.gmail.procha001.repository.impl;

import com.gmail.procha001.repository.GenericDao;
import com.gmail.procha001.repository.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable> implements GenericDao<T, ID> {

    private final Class<T> entityClass;

    public GenericDaoImpl() {
        this.entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    public ID save(T entity) {
        return (ID) getSession().save(entity);
    }

    public void saveOrUpdate(T entity) {
        getSession().saveOrUpdate(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    public T find(ID id) {
        return getSession().find(entityClass, id);
    }
}
