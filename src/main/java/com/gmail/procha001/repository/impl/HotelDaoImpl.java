package com.gmail.procha001.repository.impl;

import com.gmail.procha001.repository.HotelDao;
import com.gmail.procha001.repository.model.Hotel;

public class HotelDaoImpl  extends GenericDaoImpl<Hotel, Long> implements HotelDao {
    private static HotelDaoImpl instance;

    public static HotelDaoImpl getInstance() {
        if (instance == null) {
            instance = new HotelDaoImpl();
        }
        return instance;
    }

    private HotelDaoImpl() {
    }

}