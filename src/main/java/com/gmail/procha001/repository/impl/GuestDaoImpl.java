package com.gmail.procha001.repository.impl;

import com.gmail.procha001.repository.GuestDao;
import com.gmail.procha001.repository.model.Guest;

public class GuestDaoImpl extends GenericDaoImpl<Guest, Long> implements GuestDao {
    private static GuestDaoImpl instance;

    public static GuestDaoImpl getInstance() {
        if (instance == null) {
            instance = new GuestDaoImpl();
        }
        return instance;
    }

    private GuestDaoImpl() {
    }
}
