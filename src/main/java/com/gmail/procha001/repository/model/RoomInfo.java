package com.gmail.procha001.repository.model;


import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_ROOM_INFO")
public class RoomInfo implements Serializable {
    @Id
    @GenericGenerator(name = "generator",
            strategy = "foreign",
            parameters = @Parameter(name = "property", value = "room"))
    @GeneratedValue(generator = "generator")
    @Column(name = "F_ID")
    private Long id;
    @Column(name = "F_QUANTITY_MAN")
    private Integer quantityMan;
    @Column(name = "F_SHOWER")
    private Boolean shower;
    @Column(name = "F_TV")
    private Boolean tv;
    @OneToOne
    @PrimaryKeyJoinColumn
    private Room room;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantityMan() {
        return quantityMan;
    }

    public void setQuantityMan(Integer quantityMan) {
        this.quantityMan = quantityMan;
    }

    public Boolean getShower() {
        return shower;
    }

    public void setShower(Boolean shower) {
        this.shower = shower;
    }

    public Boolean getTv() {
        return tv;
    }

    public void setTv(Boolean tv) {
        this.tv = tv;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "RoomInfo{" +
                "id=" + id +
                ", quantityMan=" + quantityMan +
                ", shower=" + shower +
                ", tv=" + tv +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomInfo roomInfo = (RoomInfo) o;

        if (getId() != null ? !getId().equals(roomInfo.getId()) : roomInfo.getId() != null) return false;
        if (getQuantityMan() != null ? !getQuantityMan().equals(roomInfo.getQuantityMan()) : roomInfo.getQuantityMan() != null)
            return false;
        if (getShower() != null ? !getShower().equals(roomInfo.getShower()) : roomInfo.getShower() != null)
            return false;
        if (getTv() != null ? !getTv().equals(roomInfo.getTv()) : roomInfo.getTv() != null) return false;
        return getRoom() != null ? getRoom().equals(roomInfo.getRoom()) : roomInfo.getRoom() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getQuantityMan() != null ? getQuantityMan().hashCode() : 0);
        result = 31 * result + (getShower() != null ? getShower().hashCode() : 0);
        result = 31 * result + (getTv() != null ? getTv().hashCode() : 0);
        result = 31 * result + (getRoom() != null ? getRoom().hashCode() : 0);
        return result;
    }
}
