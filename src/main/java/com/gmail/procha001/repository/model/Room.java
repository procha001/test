package com.gmail.procha001.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "T_ROOM")
public class Room implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "F_ID")
    private Long id;
    @Column(name = "F_NUMBER")
    private Long number;
    @Column(name = "F_DESCRIPTION")
    private String description;
    @Column(name = "F_COAST")
    private Long coast;

    @OneToOne(mappedBy = "room", orphanRemoval = true, cascade = CascadeType.ALL)
    private RoomInfo roomInfo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "F_HOTEL_ID")
    private Hotel hotel;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "T_ROOM_GUEST",
    joinColumns = @JoinColumn(name = "F_ROOM_ID"),
    inverseJoinColumns = @JoinColumn(name = "F_GUEST_ID"))
    private List<Guest> guests=new ArrayList<Guest>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCoast() {
        return coast;
    }

    public void setCoast(Long coast) {
        this.coast = coast;
    }

    public RoomInfo getRoomInfo() {
        return roomInfo;
    }

    public void setRoomInfo(RoomInfo roomInfo) {
        this.roomInfo = roomInfo;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", number=" + number +
                ", description='" + description + '\'' +
                ", coast=" + coast +
                ", roomInfo=" + roomInfo +
                ", hotel=" + hotel +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        if (getId() != null ? !getId().equals(room.getId()) : room.getId() != null) return false;
        if (getNumber() != null ? !getNumber().equals(room.getNumber()) : room.getNumber() != null) return false;
        if (getDescription() != null ? !getDescription().equals(room.getDescription()) : room.getDescription() != null)
            return false;
        if (getCoast() != null ? !getCoast().equals(room.getCoast()) : room.getCoast() != null) return false;
        if (getRoomInfo() != null ? !getRoomInfo().equals(room.getRoomInfo()) : room.getRoomInfo() != null)
            return false;
        if (getHotel() != null ? !getHotel().equals(room.getHotel()) : room.getHotel() != null) return false;
        return getGuests() != null ? getGuests().equals(room.getGuests()) : room.getGuests() == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getNumber() != null ? getNumber().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getCoast() != null ? getCoast().hashCode() : 0);
        result = 31 * result + (getRoomInfo() != null ? getRoomInfo().hashCode() : 0);
        result = 31 * result + (getHotel() != null ? getHotel().hashCode() : 0);
        result = 31 * result + (getGuests() != null ? getGuests().hashCode() : 0);
        return result;
    }
}
