package com.gmail.procha001.repository;

import com.gmail.procha001.repository.impl.RoomDaoImpl;
import com.gmail.procha001.repository.model.Guest;
import com.gmail.procha001.repository.model.Hotel;
import com.gmail.procha001.repository.model.Room;
import com.gmail.procha001.repository.model.RoomInfo;
import com.gmail.procha001.repository.util.HibernateUtil;
import com.gmail.procha001.service.RoomService;
import com.gmail.procha001.service.impl.RoomServiceImpl;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GeneratorDB {
    private static final Logger logger = Logger.getLogger(GeneratorDB.class);
    private static GeneratorDB instance;

    public static GeneratorDB getInstance() {
        if (instance == null) {
            instance = new GeneratorDB();
        }
        return instance;
    }

    public GeneratorDB() {
        generate();
    }

    public static void generate() {
        Random rd = new Random();
        Hotel h1 = new Hotel();
        h1.setName("Hotel 1");
        h1.setRating(rd.nextInt(2 + rd.nextInt(8)));
        Hotel h2 = new Hotel();
        h2.setName("Hotel 2");
        h2.setRating(rd.nextInt(2 + rd.nextInt(8)));
        List<Room> roomList = new ArrayList<Room>();
        for (int i = 0; i < 10; i++) {
            Room room = new Room();
            room.setNumber(1L + rd.nextInt(50));
            room.setDescription("Description" + i);
            room.setCoast(1L + rd.nextInt(1000));
            RoomInfo roomInfo = new RoomInfo();
            roomInfo.setQuantityMan(1 + rd.nextInt(5));
            roomInfo.setShower(rd.nextBoolean());
            roomInfo.setTv(rd.nextBoolean());
            roomInfo.setRoom(room);
            room.setRoomInfo(roomInfo);
            if (i % 2 == 0) {
                room.setHotel(h1);
                h1.getRooms().add(room);

            } else {
                room.setHotel(h2);
                h2.getRooms().add(room);
            }
            roomList.add(room);
        }
        for (int i = 0; i < 20; i++) {
            Guest guest = new Guest();
            guest.setName("NAME" + i);
            guest.setEmail("mail" + i + "@email.us");
            int i1 = rd.nextInt(5);
            for (int j = 0; j < i1; j++) {
                Room room = roomList.get(rd.nextInt(roomList.size()));
                room.getGuests().add(guest);
                guest.getRooms().add(room);
            }
        }
        RoomDao roomDao = RoomDaoImpl.getInstance();
        roomDao.save(roomList);
        logger.info("Generated content DB");
    }
}

