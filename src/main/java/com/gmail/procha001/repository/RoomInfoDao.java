package com.gmail.procha001.repository;

import com.gmail.procha001.repository.model.RoomInfo;

public interface RoomInfoDao extends GenericDao<RoomInfo, Long> {
}
