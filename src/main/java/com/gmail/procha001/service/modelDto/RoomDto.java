package com.gmail.procha001.service.modelDto;

public class RoomDto {

    private Long id;

    private Long number;

    private String description;

    private Long coast;

    private Integer quantityMan;

    private Boolean shower;

    private Boolean tv;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCoast() {
        return coast;
    }

    public void setCoast(Long coast) {
        this.coast = coast;
    }

    public Integer getQuantityMan() {
        return quantityMan;
    }

    public void setQuantityMan(Integer quantityMan) {
        this.quantityMan = quantityMan;
    }

    public Boolean getShower() {
        return shower;
    }

    public void setShower(Boolean shower) {
        this.shower = shower;
    }

    public Boolean getTv() {
        return tv;
    }

    public void setTv(Boolean tv) {
        this.tv = tv;
    }

    @Override
    public String toString() {
        return "RoomDto{" +
                "id=" + id +
                ", number=" + number +
                ", description='" + description + '\'' +
                ", coast=" + coast +
                ", quantityMan=" + quantityMan +
                ", shower=" + shower +
                ", tv=" + tv +

                '}';
    }
}