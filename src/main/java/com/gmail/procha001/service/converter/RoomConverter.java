package com.gmail.procha001.service.converter;

import com.gmail.procha001.repository.model.Room;
import com.gmail.procha001.service.modelDto.RoomDto;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class RoomConverter {
    private static final Logger logger = Logger.getLogger(RoomConverter.class);
    public static List<RoomDto> roomToRoomDto(List<Room> rooms) {
        List<RoomDto> roomDtos=new ArrayList<RoomDto>();
        for (Room room : rooms) {
            RoomDto roomDto=new RoomDto();
            roomDto.setId(room.getId());
            roomDto.setCoast(room.getCoast());
            roomDto.setDescription(room.getDescription());
            roomDto.setNumber(room.getNumber());
            roomDto.setQuantityMan(room.getRoomInfo().getQuantityMan());
            roomDto.setShower(room.getRoomInfo().getShower());
            roomDto.setTv(room.getRoomInfo().getTv());
            roomDtos.add(roomDto);
        }
        logger.info("Convert to: "+roomDtos);
        return roomDtos;
    }
}
