package com.gmail.procha001.service;

import com.gmail.procha001.service.modelDto.RoomDto;

import java.util.List;

public interface RoomService {
    List<RoomDto> find(Long coastMin, Long coastMax, Integer quantityMan, Integer rating);

    List<RoomDto> findByGuest(String name);
}
