package com.gmail.procha001.service.impl;

import com.gmail.procha001.repository.RoomDao;
import com.gmail.procha001.repository.impl.RoomDaoImpl;
import com.gmail.procha001.service.RoomService;
import com.gmail.procha001.service.converter.RoomConverter;
import com.gmail.procha001.service.modelDto.RoomDto;

import java.util.List;

public class RoomServiceImpl implements RoomService {
    private static RoomServiceImpl instance;

    public static RoomServiceImpl getInstance() {
        if (instance == null) {
            instance = new RoomServiceImpl();
        }
        return instance;
    }

    private RoomServiceImpl() {
    }

    public List<RoomDto> find(Long coastMin, Long coastMax, Integer quantityMan, Integer rating) {
        RoomDao roomDao = RoomDaoImpl.getInstance();
        return RoomConverter.roomToRoomDto(roomDao.find(coastMin, coastMax, quantityMan, rating));
    }

    public List<RoomDto> findByGuest(String name) {
        RoomDao roomDao = RoomDaoImpl.getInstance();
        return RoomConverter.roomToRoomDto(roomDao.findByGuest(name));
    }
}
