package com.gmail.procha001.controller;

import com.gmail.procha001.repository.GeneratorDB;
import com.gmail.procha001.repository.RoomDao;
import com.gmail.procha001.repository.impl.RoomDaoImpl;
import com.gmail.procha001.repository.model.Room;
import com.gmail.procha001.service.RoomService;
import com.gmail.procha001.service.converter.RoomConverter;
import com.gmail.procha001.service.impl.RoomServiceImpl;
import com.gmail.procha001.service.modelDto.RoomDto;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class StartServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(StartServlet.class);
    private final RoomService roomService = RoomServiceImpl.getInstance();
    private final GeneratorDB generatorDB = GeneratorDB.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("guestName");
        Long coastMin = null;
        Long coastMax = null;
        Integer quantityMan = null;
        Integer rating = null;
        if (req.getParameter("coastMin") != null) {
            coastMin = Long.parseLong(req.getParameter("coastMin"));
        }
        if (req.getParameter("coastMax") != null) {
            coastMax = Long.parseLong(req.getParameter("coastMax"));
        }
        if (req.getParameter("quantityMan") != null) {
            quantityMan = Integer.parseInt(req.getParameter("quantityMan"));
        }
        if (req.getParameter("rating") != null) {
            rating = Integer.parseInt(req.getParameter("rating"));
        }
        logger.info("Parsing fields: " + name + " " + coastMin + " " + coastMax + " " + quantityMan + " " + rating);
        if (name != null) {
            List<RoomDto> rooms = roomService.findByGuest(name);
            req.setAttribute("rooms", rooms);
            req.getRequestDispatcher("/WEB-INF/pages/StartPage.jsp").forward(req, resp);
        }
        if (coastMin != null && coastMax != null && quantityMan != null && rating != null) {
            List<RoomDto> rooms = roomService.find(coastMin, coastMax, quantityMan, rating);
            req.setAttribute("rooms", rooms);
            req.getRequestDispatcher("/WEB-INF/pages/StartPage.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/WEB-INF/pages/StartPage.jsp").forward(req, resp);
        }
    }
}