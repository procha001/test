package com.gmail.procha001.repository.impl;


import com.gmail.procha001.repository.GeneratorDB;
import com.gmail.procha001.repository.RoomDao;
import com.gmail.procha001.repository.model.Room;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

public class RoomDaoImplTest {

    @Test
    public void gfd() {
        GeneratorDB generatorDB=GeneratorDB.getInstance();    }

    @Test
    public void testFind() {
        RoomDao roomDao = RoomDaoImpl.getInstance();
        List<Room> rooms = roomDao.find(20L, 1000L, 2, 5);
        assertNotNull(rooms);
    }

    @Test
    public void testFindByGuest() {
        RoomDao roomDao = RoomDaoImpl.getInstance();
        List<Room> rooms1 = roomDao.findByGuest("NAME6");
        List<Room> rooms = roomDao.find(20L, 1000L, 2, 5);
        assertNotNull(rooms1);
    }
}
