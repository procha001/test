package com.gmail.procha001.service.converter;

import com.gmail.procha001.repository.RoomDao;
import com.gmail.procha001.repository.impl.RoomDaoImpl;
import com.gmail.procha001.repository.model.Room;
import com.gmail.procha001.service.modelDto.RoomDto;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RoomConverterTest {
    @Test
    public void testRoomToRooDto(){
        RoomDao roomDao = RoomDaoImpl.getInstance();
        List<Room> rooms = roomDao.find(20L, 1000L, 2, 5);
        List<RoomDto> roomDtos=RoomConverter.roomToRoomDto(rooms);
        assertNotNull(roomDtos);
    }

}